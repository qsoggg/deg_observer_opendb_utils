-- public.region_ik definition

-- Drop table

-- DROP TABLE public.region_ik;

CREATE TABLE public.region_ik (
	name_region varchar(100) NULL,
	id serial4 NOT NULL,
	code int4 NULL,
	ik_name varchar(100) NULL
);
CREATE UNIQUE INDEX pk_tik ON public.region_ik USING btree (id);
CREATE UNIQUE INDEX unq_tik_id ON public.region_ik USING btree (id);


-- public.voting definition

-- Drop table

-- DROP TABLE public.voting;

CREATE TABLE public.voting (
	sc_type varchar(20) NULL,
	region_ik_id int4 NULL,
	id serial4 NOT NULL,
	"name" varchar(100) NULL,
	sc_id varchar(100) NULL,
	bulletinhash varchar(100) NULL,
	timecreation timestamp NULL,
	timestart timestamp NULL,
	timeissuefinish timestamp NULL,
	timeballotfinish timestamp NULL,
	CONSTRAINT voting_pkey PRIMARY KEY (id),
	CONSTRAINT voting_region_ik_id_fkey FOREIGN KEY (region_ik_id) REFERENCES public.region_ik(id)
);
CREATE UNIQUE INDEX pk_votings ON public.voting USING btree (id);
CREATE UNIQUE INDEX unq_votings_id ON public.voting USING btree (id);


-- public.voting_list_event definition

-- Drop table

-- DROP TABLE public.voting_list_event;

CREATE TABLE public.voting_list_event (
	id serial4 NOT NULL,
	event_type int4 NULL,
	"time" timestamp NULL,
	tx_id int4 NULL,
	voting_id int4 NULL,
	CONSTRAINT voting_list_event_pkey PRIMARY KEY (id),
	CONSTRAINT voting_list_event_voting_id_fkey FOREIGN KEY (voting_id) REFERENCES public.voting(id)
);


-- public.ballot_in definition

-- Drop table

-- DROP TABLE public.ballot_in;

CREATE TABLE public.ballot_in (
	id serial4 NOT NULL,
	ballot varchar(200) NULL,
	voting_id int4 NULL,
	"time" timestamp NULL,
	tx_id int4 NULL,
	voterpublickey varchar(100) NULL,
	CONSTRAINT ballot_in_voting_id_fkey FOREIGN KEY (voting_id) REFERENCES public.voting(id)
);
CREATE UNIQUE INDEX pk_bullots ON public.ballot_in USING btree (id);


-- public.result_event definition

-- Drop table

-- DROP TABLE public.result_event;

CREATE TABLE public.result_event (
	id serial4 NOT NULL,
	voting_id int4 NULL,
	result_array varchar(100) NULL,
	"time" timestamp NULL,
	tx_id varchar(100) NULL,
	CONSTRAINT result_event_pkey PRIMARY KEY (id),
	CONSTRAINT result_event_voting_id_fkey FOREIGN KEY (voting_id) REFERENCES public.voting(id)
);


-- public.result_item definition

-- Drop table

-- DROP TABLE public.result_item;

CREATE TABLE public.result_item (
	id serial4 NOT NULL,
	voting_id int4 NULL,
	candidate_result int8 NULL,
	candidate_id int4 NULL,
	result_event_id int4 NULL,
	CONSTRAINT result_item_pkey PRIMARY KEY (id),
	CONSTRAINT result_item_result_event_id_fkey FOREIGN KEY (result_event_id) REFERENCES public.result_event(id),
	CONSTRAINT result_item_voting_id_fkey FOREIGN KEY (voting_id) REFERENCES public.voting(id)
);
CREATE UNIQUE INDEX pk_rusults ON public.result_item USING btree (id);


-- public.voter definition

-- Drop table

-- DROP TABLE public.voter;

CREATE TABLE public.voter (
	voter_hash varchar(100) NULL,
	id serial4 NOT NULL,
	voting_id int4 NULL,
	CONSTRAINT voter_voting_id_fkey FOREIGN KEY (voting_id) REFERENCES public.voting(id)
);
CREATE UNIQUE INDEX pk_voters ON public.voter USING btree (id);


-- public.voting_list_event_item definition

-- Drop table

-- DROP TABLE public.voting_list_event_item;

CREATE TABLE public.voting_list_event_item (
	id serial4 NOT NULL,
	add_delete int4 NULL,
	vl_event int4 NULL,
	voting_id int4 NULL,
	voter_id int4 NULL,
	uik int4 NULL,
	CONSTRAINT voting_list_event_item_pkey PRIMARY KEY (id),
	CONSTRAINT voting_list_event_item_vl_event_fkey FOREIGN KEY (vl_event) REFERENCES public.voting_list_event(id),
	CONSTRAINT voting_list_event_item_voter_id_fkey FOREIGN KEY (voter_id) REFERENCES public.voter(id),
	CONSTRAINT voting_list_event_item_voting_id_fkey FOREIGN KEY (voting_id) REFERENCES public.voting(id)
);


-- public.ballot_out definition

-- Drop table

-- DROP TABLE public.ballot_out;

CREATE TABLE public.ballot_out (
	id serial4 NOT NULL,
	voter_id int4 NULL,
	"time" timestamp NULL,
	voting_id int4 NULL,
	uik int4 NULL,
	tx_id varchar(100) NULL,
	CONSTRAINT ballot_out_pkey PRIMARY KEY (id),
	CONSTRAINT ballot_out_voter_id_fkey FOREIGN KEY (voter_id) REFERENCES public.voter(id),
	CONSTRAINT ballot_out_voting_id_fkey FOREIGN KEY (voting_id) REFERENCES public.voting(id)
);